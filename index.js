(function main() {

    let heartBeatingStop;
    let shakingStop;
    let moveAndHideReset;

    document.getElementById('fadeInPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('fadeInBlock');
            getAnimaster().addFadeIn(5000).play(block);
        });
    // вместо move второй блок делает анимацию из 4 задания с параметрами из примера про customAnimation через play
    document.getElementById('movePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('moveBlock');
            getAnimaster()
                .addMove(200, { x: 40, y: 40 })
                .addScale(800, 1.3)
                .addMove(200, { x: 80, y: 0 })
                .addScale(800, 1)
                .addMove(200, { x: 40, y: -40 })
                .addScale(800, 0.7)
                .addMove(200, { x: 0, y: 0 })
                .addScale(800, 1)
                .play(block);
        });

    //вместо scale третий блок делает анимацию из 7 задания с параметрами из примера про buildHandler через buildHandler
    const scalePlayHandler = getAnimaster()
        .addMove(200, { x: 80, y: 0 })
        .addMove(200, { x: 0, y: 0 })
        .addMove(200, { x: 80, y: 0 })
        .addMove(200, { x: 0, y: 0 })
        .buildHandler('scaleBlock');

    document.getElementById('scalePlay')
        .addEventListener('click', scalePlayHandler);

    document.getElementById('fadeOutPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('fadeOutBlock');
            getAnimaster().addFadeOut(5000).play(block);
        });

    document.getElementById('moveAndHidePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('moveAndHideBlock');
            moveAndHideReset = getAnimaster().moveAndHide(block, 5000);
        });

    document.getElementById('showAndHidePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('showAndHideBlock');
            getAnimaster().showAndHide(block, 3000);
        });

    document.getElementById('heartBeatingPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('heartBeatingBlock');
            heartBeatingStop = getAnimaster().heartBeating(block, 500);
        });

    document.getElementById('shakingPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('shakingBlock');
            shakingStop = getAnimaster().shaking(block, 500);
        });

    document.getElementById('heartBeatingStop')
        .addEventListener('click', function () {
            heartBeatingStop.stop();
        });
    document.getElementById('heartBeatingReset')
        .addEventListener('click', function () {
            heartBeatingStop.reset();
        });

    document.getElementById('shakingStop')
        .addEventListener('click', function () {
            shakingStop.stop();
        });
    document.getElementById('shakingReset')
        .addEventListener('click', function () {
            shakingStop.reset();
        });

    document.getElementById('moveAndHideReset')
        .addEventListener('click', function () {
            const block = document.getElementById('moveAndHideBlock');
            moveAndHideReset.reset(block);
        });
})();

let animaster = getAnimaster();

function getAnimaster() {


    let animaster = {
        _steps: [],
    };

    function Step(name, duration, params) {
        this.name = name;
        this.duration = duration;
        this.params = params;
    };

    animaster.addAnimation = function (name, duration, params) {
        this._steps.push(new Step(name, duration, params));
        return this;
    };

    animaster.addMove = function (duration, params) {
        this.addAnimation('move', duration, params);
        return this;
    };
    animaster.addScale = function (duration, params) {
        this.addAnimation('scale', duration, params);
        return this;
    };
    animaster.addFadeIn = function (duration) {
        this.addAnimation('fadeIn', duration);
        return this;
    };
    animaster.addFadeOut = function (duration) {
        this.addAnimation('fadeOut', duration);
        return this;
    };
    animaster.addDelay = function (duration) {
        this.addAnimation('delay', duration);
        return this;
    }

    animaster._cycled = false;
    animaster._timers = [];
    animaster.previousScale = 0;
    animaster.previousPosition = 0;

    animaster.buildHandler = function (blockId) {
        let innerHandler = () => {
            const block = document.getElementById(blockId);
            this.play(block);
        };
        return innerHandler;
    }

    animaster.play = function (element, cycled) {
        this._cycled = cycled;
        let time = 0;
        let startVisibility = element.classList.contains("show");
        let animation = {};

        animation.stop = () => {
            this._cycled = false;
            for (let i = 0; i < this._timers.length; i++) {
                clearTimeout(this._timers[i]);
            };
        };

        animation.reset = function () {
            element.style.transform = null;
            element.style.transitionDuration = null;

            if (startVisibility) {
                element.classList.remove('hide');
                element.classList.add('show');
            }
            else {
                element.classList.remove('hide');
                element.classList.add('show');
            }
            animation.stop();
        };

        for (let i = 0; i < this._steps.length; i++) {

            if (this._steps[i].name === 'move') {
                let timerId = setTimeout(() => {
                    this._move(element, this._steps[i].duration, this._steps[i].params);
                    this.previousPosition = this._steps[i].params;
                }, time);
                this._timers.push(timerId);
            }
            else if (this._steps[i].name === 'scale') {
                let timerId = setTimeout(() => {
                    this._scale(element, this._steps[i].duration, this._steps[i].params);
                    this.previousScale = this._steps[i].params;
                }, time);
                this._timers.push(timerId);
            }
            else if (this._steps[i].name === 'fadeIn') {
                let timerId = setTimeout(() => this._fadeIn(element, this._steps[i].duration), time);
                this._timers.push(timerId);
            }
            else if (this._steps[i].name === 'fadeOut') {
                let timerId = setTimeout(() => this._fadeOut(element, this._steps[i].duration), time);
                this._timers.push(timerId);
            }
            else if (this._steps[i].name === 'delay') {
            }
            time += this._steps[i].duration;
        }
        if (this._cycled) {
            let timerId = setTimeout(() => this.play(element, this._cycled), time);
            this._timers.push(timerId);
        };
        return animation;
    };

    animaster.move = function (element, duration, translation) {
        this.addMove(duration, translation).play(element);
    };

    animaster._move = function (element, duration, translation) {
        element.style.transitionDuration = `${duration}ms`;
        element.style.transform = getTransform(translation, this.previousScale);
    };

    animaster._scale = function (element, duration, ratio) {
        element.style.transitionDuration = `${duration}ms`;
        element.style.transform = getTransform(this.previousPosition, ratio);
    };
    animaster._fadeIn = function (element, duration) {
        element.style.transitionDuration = `${duration}ms`;
        element.classList.remove('hide');
        element.classList.add('show');
    };
    animaster._fadeOut = function (element, duration) {
        element.style.transitionDuration = `${duration}ms`;
        element.classList.remove('show');
        element.classList.add('hide');
    };

    animaster.moveAndHide = function (element, duration) {
        return this.addMove(duration * 2 / 5, { x: 100, y: 20 }).addFadeOut(duration * 3 / 5).play(element);
    };

    animaster.showAndHide = function (element, duration) {
        this.addFadeIn(duration / 3).addDelay(duration / 3).addFadeOut(duration / 3).play(element);
    };

    animaster.heartBeating = function (element, duration) {
        return this.addScale(duration, 1.4).addScale(duration, 0.6).play(element, true);
    };

    // animaster.shaking = function (element, duration) {
    //     let timerId = setInterval(() => {
    //         this.move(element, duration, { x: -20, y: 0 })
    //         setTimeout(() => { this.move(element, duration, { x: 20, y: 0 }) }, duration);
    //     }, duration * 2);
    //     let shaking = {};
    //     shaking.stop = function () {
    //         clearInterval(timerId);
    //     }
    //     return shaking;
    // };


    animaster.shaking = function (element, duration) {
        return this
            .addMove(duration / 2, { x: 20, y: 0 })
            .addMove(duration / 2, { x: 0, y: 0 })
            .play(element, true)
    }

    animaster.resetFadeIn = function (element) {
        element.classList.remove('show');
        element.classList.add('hide');
        element.style.transitionDuration = null;
    };

    animaster.resetFadeOut = function (element) {
        element.classList.remove('hide');
        element.classList.add('show');
        element.style.transitionDuration = null;
    };

    animaster.resetMoveAndScale = function (element) {
        element.style.transitionDuration = null;
        element.style.transform = null;
    };

    return animaster;
};

function getTransform(translation, ratio) {
    const result = [];
    if (translation) {
        result.push(`translate(${translation.x}px,${translation.y}px)`);
    }
    if (ratio) {
        result.push(`scale(${ratio})`);
    }
    return result.join(' ');
};
